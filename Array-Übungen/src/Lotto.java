
public class Lotto {

	public static void main(String[] args) {
		
		int lotto [] = {3,7,12,18,37,42};
		
		System.out.print("[ ");
		for (int i = 0; i < lotto.length; i++) {
			System.out.print(lotto[i] + " ");
			
		}
		System.out.print("] ");
		
		int gesuchteZahl;
		boolean zahlVorhanden;
		
		gesuchteZahl = 12;
		zahlVorhanden = false;
		for (int i = 0; i < lotto.length; i++) {
			if(lotto[i] == gesuchteZahl) {
				zahlVorhanden = true;				
				break;
			}
		}
		if(zahlVorhanden) {
			System.out.println("Die gesuchte Zahl " + gesuchteZahl + " ist vorhanden.");		
		}
		else {
			System.out.println("Die gesuchte Zahl " + gesuchteZahl + " ist nicht vorhanden.");
		}
		
		gesuchteZahl = 13;
		zahlVorhanden = false;
		for (int i = 0; i < lotto.length; i++) {
			if(lotto[i] == gesuchteZahl) {
				zahlVorhanden = true;				
				break;
			}
		}
		if(zahlVorhanden) {
			System.out.println("Die gesuchte Zahl " + gesuchteZahl + " ist vorhanden.");		
		}
		else {
			System.out.println("Die gesuchte Zahl " + gesuchteZahl + " ist nicht vorhanden.");
		}
		
	}
}
