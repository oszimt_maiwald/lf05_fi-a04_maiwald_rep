﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		String [] fahrkartenname = new String[10];
		fahrkartenname[0] = "Einzelfahrschein Berlin AB";
		fahrkartenname[1] = "Einzelfahrschein Berlin BC";
		fahrkartenname[2] = "Einzelfahrschein Berlin ABC";
		fahrkartenname[3] = "Kurzstrecke";
		fahrkartenname[4] = "Tageskarte Berlin AB";
		fahrkartenname[5] = "Tageskarte Berlin BC";
		fahrkartenname[6] = "Tageskarte Berlin ABC";
		fahrkartenname[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenname[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenname[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		double [] fahrkartenpreis = new double[10];
		fahrkartenpreis[0] = 2.90;
		fahrkartenpreis[1] = 3.30;
		fahrkartenpreis[2] = 3.60;
		fahrkartenpreis[3] = 1.90;
		fahrkartenpreis[4] = 8.60;
		fahrkartenpreis[5] = 9.00;
		fahrkartenpreis[6] = 9.60;
		fahrkartenpreis[7] = 23.50;
		fahrkartenpreis[8] = 24.30;
		fahrkartenpreis[9] = 24.90;
		
		System.out.println("Bitte Fahrkarte auswählen: ");
		for(int i = 0; i < fahrkartenname.length; i++) {
			System.out.println(i+1 + " - " + fahrkartenname[i] + " - " + fahrkartenpreis[i]);			
		}
		int i = tastatur.nextInt() - 1;
		double zuZahlenderBetrag = fahrkartenpreis[i];
		
		System.out.print("Anzahl Tickets eingeben: ");
		int anzahlTickets = tastatur.nextInt(); // nextInt(), da anzahlTickets vom Datentyp int ist 
		if (anzahlTickets >= 1 && anzahlTickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): %.2f\n", (zuZahlenderBetrag * anzahlTickets) );
			}
		else {
			System.out.println("Die Anzahl der Tickets ist ungültig. Die Anzahl der Tickets wurde auf \"1\" gesetzt.\n");
			anzahlTickets = 1;
			System.out.printf("Zu zahlender Betrag (EURO): %.2f\n", (zuZahlenderBetrag * anzahlTickets) );
			}
		
		zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
		return zuZahlenderBetrag;
		
	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		// Geldeinwurf
		// -----------
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.printf("Noch zu zahlen: %.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch blocks
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		// double rückgabebetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");

	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double rückgabebetrag;
		int neustart = 1;
		
		do {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);
		}
		while (neustart == 1);
	}
}