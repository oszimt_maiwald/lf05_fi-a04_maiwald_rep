import java.util.Scanner;

public class Aufgabe4 {
	
	public static void main(String[] args) {
		int zahl = 5;
		System.out.print(romanNumerals(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	public static char romanNumerals(int zahl) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie eine Ganzzahl ein: ");
		zahl = tastatur.nextInt();
		char roman = '?'; 
		if (zahl == 1) {
			roman = 'I';
		}
		else if (zahl == 5) {
			roman = 'V';
		}
		else if (zahl == 10) {
			roman = 'X';
		}
		else if (zahl == 50) {
			roman = 'L';
		}
		else if (zahl == 100) {
			roman = 'C';
		}
		else if (zahl == 500) {
			roman = 'D';
		}
		else if (zahl == 1000) {
			roman = 'M';
		}
		else {
			roman = '?';
		}
		return roman;
	}
}
